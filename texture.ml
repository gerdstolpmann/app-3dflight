(* $Id$
 * (C) 2005 by Gerd Stolpmann. See the file LICENSE for your rights.
 *)

let wall_texture =
  lazy (
    (* Achtung: GlPix.of_raw kann mit alignments nicht umgehen. Daher muss
     * eine Texturgr��e nehmen, bei der alignment keine Rolle spielt.
     * Bei RGB muss die Breite durch 3 und 4 teilbar sein; bei RGBA durch
     * 4. 
     * Andererseits erlaubt lablgl ohnehin nur Texturen, bei denen H�he
     * und Breite eine Zweierpotenz ist. Daher sind wir gezwungen, das
     * RGBA-Format zu benutzen.
     *)
    let texture_surf1 = Sdlloader.load_image "Textur.jpg" in
    let (w,h,_) = Sdlvideo.surface_dims texture_surf1 in
    let texture_surf2 = Sdlvideo.create_RGB_surface 
			  [] ~w ~h ~bpp:32
			  ~rmask:0xffl ~gmask:0xff00l ~bmask:0xff0000l
			  ~amask:0xff000000l in
    Sdlvideo.blit_surface ~src:texture_surf1 ~dst:texture_surf2 ();
    let texture_raw = Sdlgl.to_raw texture_surf2 in
    let texture_pix = GlPix.of_raw texture_raw `rgba w h in
    let t = GlTex.gen_texture() in
    GlTex.bind_texture `texture_2d t;
    GlTex.parameter `texture_2d (`wrap_s `repeat);
    GlTex.parameter `texture_2d (`wrap_t `repeat);
    GlTex.parameter `texture_2d (`min_filter `nearest);
    GlTex.parameter `texture_2d (`mag_filter `nearest);
    GlTex.image2d ~internal:2 texture_pix;
    t
  )
;;
