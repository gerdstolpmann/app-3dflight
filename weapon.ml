(* $Id$
 * (C) 2005 by Gerd Stolpmann. See the file LICENSE for your rights.
 *)

open Vectorops;;
open Auto_obj;;
open Printf;;

class weapon ?(lifetime = 1000) t0 p0 s0 : auto_obj =
  (* The weapon starts at time t0, position p0 with speed vector s0 *)
  (* Speed vector is distance per second, not millisecond *)

  let (angle_z, n_z) =
    (* The angle of s0 with the positive z axis, and the normal vector
     * of s0 and the z axis.
     *)
    let angle = degrees(vangle s0 (0.0, 0.0, 1.0)) in
    let n = vnormal (0.0, 0.0, 1.0) s0 in
    if vlength n < 0.00001 then
      (* approx n is the zero vector *)
      (180.0, (0.0, 1.0, 0.0))
    else
      (angle, n)
  in

object(self)
  val mutable t_cur = t0
  val quad = GluQuadric.create()

  method global_dlist = None

  method private ppos t =
    (float (t - t0) *. 0.001) *.^ s0 +^ p0

  method point_position t =
    Some (self#ppos t)

  method collision t p = false

  method update t ep =
    if t - t0 > lifetime then raise Remove;
    t_cur <- t

  method is_visible = 
    true

  method dl_change = 
    false
      
  method draw() =
    let p = self # ppos t_cur in
    (* Draw a cylinder from p against the direction of s0 with a certain
     * length
     *)
    let col = (1.0, 0.0, 0.0) in
    GlLight.material `both (`specular (vexpand col));
    GlLight.material `both (`shininess 100.0);
    GlDraw.color col;
    GlDraw.polygon_mode `both `fill;
    GlMat.load_identity();
    (* CHECK: sign of [angle] may be wrong *)
    GlMat.translate3 p;
    GlMat.rotate3 ~angle:angle_z n_z;
    GlMat.translate3 (0.0, 0.0, -1.0);
    GluQuadric.normals quad `smooth;
    GluQuadric.orientation quad `outside;
    GluQuadric.cylinder 
      ~base:0.002 (* radius *) 
      ~top:0.002  (* radius *) 
      ~height:1.0 ~slices:16 ~stacks:2 ~quad ();
    (* Draws a cylinder along the z axis, base at z=0, top at z=height *)
(*    let m = GlMat.get_matrix `modelview_matrix in
    GlMat.load_identity();
    GlMat.rotate3 ~angle:180.0 (0.0, 1.0, 0.0);  (* flip face *)
    GlMat.mult m;
*)
    GluQuadric.orientation quad `inside;  (* flip face *)
    GluQuadric.disk
      ~inner:0.0 (* radius *)
      ~outer:0.002 (* radius *)
      ~slices:16
      ~loops:1 ~quad ();

end
;;
