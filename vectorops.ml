(* $Id$
 * (C) 2005 by Gerd Stolpmann. See the file LICENSE for your rights.
 *)

(* Vectors of R3 *)

let ( +^ ) (a1,a2,a3) (b1,b2,b3) = (a1+.b1, a2+.b2, a3+.b3);;
let ( -^ ) (a1,a2,a3) (b1,b2,b3) = (a1-.b1, a2-.b2, a3-.b3);;
let ( *.^ ) r (a1,a2,a3) = (r*.a1, r*.a2, r*.a3);;
    (* scalar * vector *)

let ( *< ) (a1,a2,a3) (b1,b2,b3) = a1*.b1 +. a2*.b2 +. a3*.b3;;
    (* scalar product
     * read "*<" as: the product that determines the angle
     *)

let ( *^^ ) (a1,a2,a3) (b1,b2,b3) = (a2 *. b3 -. a3 *. b2,
				     a3 *. b1 -. a1 *. b3,
				     a1 *. b2 -. a2 *. b1);;
    (* vector product *)

let vlength (a1,a2,a3) = sqrt(a1*.a1 +. a2*.a2 +. a3*.a3);;

let almost_null = 0.0000001 ;;

let vone x = 
  (* vector of length 1, but same direction *)
  let l = vlength x in
  if l < almost_null then
    (0.0, 0.0, 0.0)
  else
    (1.0 /. l) *.^ x ;;

let vexpand ?(hom = 0.0) (a,b,c) = (a,b,c,hom);;
  (* Set the homogenous part of the vector *)

let vnormal x y = 
  (* The normal vector perpendicular to x and y, with length 1 *)
  vone (x *^^ y) ;;

let vangle x y =
  (* The angle between x and y, 0 <= angle <= pi *)
  let cos_alpha = (x *< y) /. (vlength x *. vlength y) in
  acos cos_alpha
;;


let _factor = 180.0 /. 3.1415926536;;

let degrees x =
  _factor *. x
;;


(* Matrices of R3 *)

(* Matrices are in column format *)

let ( +:: ) ((a11,a21,a31),(a12,a22,a32),(a13,a23,a33))
            ((b11,b21,b31),(b12,b22,b32),(b13,b23,b33)) 
  = ((a11+.b11, a21+.b21, a31+.b31),
     (a12+.b12, a22+.b22, a32+.b32),
     (a13+.b13, a23+.b23, a33+.b33)) ;;

let ( -:: ) ((a11,a21,a31),(a12,a22,a32),(a13,a23,a33))
            ((b11,b21,b31),(b12,b22,b32),(b13,b23,b33)) 
  = ((a11-.b11, a21-.b21, a31-.b31),
     (a12-.b12, a22-.b22, a32-.b32),
     (a13-.b13, a23-.b23, a33-.b33)) ;;

let ( *.:: ) r ((a11,a21,a31),(a12,a22,a32),(a13,a23,a33)) 
  = ((r*.a11,r*.a21,r*.a31),(r*.a12,r*.a22,r*.a32),(r*.a13,r*.a23,r*.a33)) ;;
  (* Scalar times matrix *)

let ( *::^ ) ((a11,a21,a31),(a12,a22,a32),(a13,a23,a33)) (r1,r2,r3) 
  = ((a11*.r1 +. a12*.r2 +. a13*.r3),
     (a21*.r1 +. a22*.r2 +. a23*.r3),
     (a31*.r1 +. a32*.r2 +. a33*.r3)) ;;
  (* matrix times vector *)

let ( *:: ) ((a11,a21,a31),(a12,a22,a32),(a13,a23,a33))
            ((b11,b21,b31),(b12,b22,b32),(b13,b23,b33)) 
  = ((a11*.b11 +. a12*.b21 +. a13*.b31,
      a21*.b11 +. a22*.b21 +. a23*.b31,
      a31*.b11 +. a32*.b21 +. a33*.b31),
     (a11*.b12 +. a12*.b22 +. a13*.b32,
      a21*.b12 +. a22*.b22 +. a23*.b32,
      a31*.b12 +. a32*.b22 +. a33*.b32),
     (a11*.b13 +. a12*.b23 +. a13*.b33,
      a21*.b13 +. a22*.b23 +. a23*.b33,
      a31*.b13 +. a32*.b23 +. a33*.b33)) ;;
  (* matrix times matrix *)

let transmatrix ((a11,a21,a31),(a12,a22,a32),(a13,a23,a33)) =
  ((a11,a12,a13),(a21,a22,a23),(a31,a32,a33));;
  (* transposed matrix *)

let orthomatrix (a1,a2,a3) =
  (* Computes the orthonormal matrix using the Gram-Schmidt algorithm *)
  (* let w1 = a1 in *)
  let w1_s = a1 *< a1 in
  let w2 = a2 -^ ((a2 *< a1) /. w1_s) *.^ a1 in
  let w2_s = w2 *< w2 in
  let w3 = a3 -^ ((a3 *< a1) /. w1_s) *.^ a1 -^ ((a3 *< w2) /. w2_s) *.^ w2 in
  (vone a1, vone w2, vone w3)
;;


let gauss ((a_11,a_21,a_31),(a_12,a_22,a_32),(a_13,a_23,a_33)) (b_1,b_2,b_3) =
  (* Solves a * x = b, returns x *)

  let step3 a11 a12 a13 (* a21 *) a22 a23 (* a31 *) (* a32 *) a33 b1 b2 b3 =
    let x3 = b3 /. a33 in
    let x2 = (b2 -. a23*.x3) /. a22 in
    let x1 = (b1 -. a12*.x2 -. a13*.x3) /. a11 in
    (x1, x2, x3)
  in

  let step2_elim a11 a12 a13 (* a21 *) a22 a23 (* a31 *) a32 a33 b1 b2 b3 =
    (* Eliminate a32 *)
    let a32_a22 = a32/.a22 in
    step3 a11 a12 a13
          (* 0 *) a22 a23
          (* 0 *) (* 0 *) (a33 -. a32_a22*.a23)
          b1 b2 (b3 -. a32_a22*.b2)
  in

  let step2_pivot a11 a12 a13 (* a21 *) a22 a23 (* a31 *) a32 a33 b1 b2 b3 =
    (* Select the pivot element *)
    let x22 = abs_float a22 in
    let x32 = abs_float a32 in
    if x22 > x32 then
      (* x22 is the pivot element *)
      step2_elim a11 a12 a13 (* a21 *) a22 a23 (* a31 *) a32 a33 b1 b2 b3
    else
      (* x32 is the pivot element *)
      step2_elim a11 a12 a13 (* a31 *) a32 a33 (* a21 *) a22 a23 b1 b3 b2
  in

  let step1_elim a11 a12 a13 a21 a22 a23 a31 a32 a33 b1 b2 b3 =
    (* Eliminate a21, a31 *)
    let a21_a11 = a21 /. a11 in
    let a31_a11 = a31 /. a11 in
    step2_pivot a11 a12 a13 
                (* 0 *) (a22 -. a21_a11*.a12) (a23 -. a21_a11*.a13)
                (* 0 *) (a32 -. a31_a11*.a12) (a33 -. a31_a11*.a13)
                b1 (b2 -. a21_a11*.b1) (b3 -. a31_a11*.b1)
  in

  let step1_pivot a11 a12 a13 a21 a22 a23 a31 a32 a33 b1 b2 b3 =
    (* Select the pivot element *)
    let x11 = abs_float a11 in
    let x21 = abs_float a21 in
    let x31 = abs_float a31 in
    if x11 > x21 then (
      if x11 > x31 then
	(* x11 is the pivot element *)
	step1_elim a11 a12 a13 a21 a22 a23 a31 a32 a33 b1 b2 b3
      else
	(* x31 is the pivot element *)
	step1_elim a31 a32 a33 a21 a22 a23 a11 a12 a13 b3 b2 b1
    )
    else (
      if x21 > x31 then
	(* x21 is the pivot element *)
	step1_elim a21 a22 a23 a11 a12 a13 a31 a32 a33 b2 b1 b3
      else
	(* x31 is the pivot element *)
	step1_elim a31 a32 a33 a21 a22 a23 a11 a12 a13 b3 b2 b1
    )
  in
  step1_pivot a_11 a_12 a_13 a_21 a_22 a_23 a_31 a_32 a_33 b_1 b_2 b_3
;;


let invmatrix m =
  (* Inverts the matrix *)
  let c1 = gauss m (1.0, 0.0, 0.0) in
  let c2 = gauss m (0.0, 1.0, 0.0) in
  let c3 = gauss m (0.0, 0.0, 1.0) in
  (c1,c2,c3)
;;


let orthoproject p q =
  (* Computes a matrix m that projects points to the plane spanned by
   * the vectors p and q, i.e. to project x on to the plane, compute
   * y = m *::^ x.
   *)
  let r = vnormal p q in
  (* p, q, r are now a base of R3 *)
  let c = orthomatrix (p,q,r) in
  let c_t = transmatrix c in
  let r = ((1.0,0.0,0.0),(0.0,1.0,0.0),(0.0,0.0,0.0)) in
  (c *:: r) *:: c_t
;;


let orthoproject_tr p q =
  (* Computes a matrix m  that projects points to the plane spanned by
   * the vectors p and q, and that expresses these points as linear combination
   * of p and q (ignore the third component of the resulting vectors), i.e.
   * compute u = m *::^ x and you get
   * (u1,u2) * (p,q) = orthoproject p q
   *)
  let m = orthoproject p q in
  let r = vnormal p q in
  let n = invmatrix (p,q,r) in
  n *:: m
;;
