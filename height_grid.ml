(* $Id$
 * (C) 2005 by Gerd Stolpmann. See the file LICENSE for your rights.
 *)

open Vectorops;;
open Auto_obj;;
open Printf;;

let create_grid m n =
  let g = Array.make_matrix m n 0.0 in
  for i = 0 to m - 1 do
    for j = 0 to n - 1 do
      g.(i).(j) <- 1.5 *. (Random.float 1.0 -. 0.5)
        (* if (i+j) land 1 = 0 then 0.1 else -0.1 *)
        (* 0.2 *. sin (0.2 *. float i +. 0.1 *. float j) *)
    done
  done;
  g
;;


let position_grid ~p ~px ~py g =
  (* p, px, py are three corner points of the grid *)
  let m = Array.length g in
  let n = Array.length g.(0) in
  let zero = (0.0, 0.0, 0.0) in
  let r = Array.make_matrix m n zero in
  let xd = (1.0 /. float(m-1)) *.^ (px -^ p) in
  let yd = (1.0 /. float(n-1)) *.^ (py -^ p) in
  let q = (px -^ p) *^^ (py -^ p) in
  let qn = (1.0 /. (vlength q)) *.^ q in   (* normal vector *)
  for i = 0 to m - 1 do       (* x direction *)
    let xbase = (float i) *.^ xd +^ p in
    for j = 0 to n - 1 do     (* y direction *)
      let xybase = (float j) *.^ yd +^ xbase in
      let vertex = 
	if i > 0 && i < m - 1 && j > 0 && j < n - 1 then
	  xybase +^ (g.(i).(j) *.^ qn) 
	else
	  xybase
      in
      r.(i).(j) <- vertex
    done
  done;
  r
;;


let normal_grid g = 
  (* Normal vectors are properties of vertexes in OpenGL, not of 
   * surfaces. To get it right, we first compute the normals of 
   * the surfaces sn, then the normals of the vertexes vn.
   *
   * PICTURE: The triangles will be drawn as triangle strips for the grid:
   *
   * (i+1) +-----+-----+-----+
   *       |\ n2 |\    |\    |
   *       | \   | \   | \   |
   *       |  \  |  \  |  \  |
   *       |   \ |   \ |   \ |
   *       | n1 \|    \|    \|
   * (i)   +-----+-----+-----+
   *      (j)   (j+1) (j+2) (j+3)
   *
   * let (n1,n2) = sn.(i).(j): see picture
   *)
  let m = Array.length g in
  let n = Array.length g.(0) in
  let zero = (0.0, 0.0, 0.0) in
  let sn = Array.make_matrix (m-1) (n-1) (zero,zero) in
  for i = 0 to m - 2 do
    for j = 0 to n - 2 do
      (* Important: Choose the triangle vertexes such that the vertexes of
       * all triangles are oriented in the same way
       *)
      let a1 = g.(i).(j) in
      let b1 = g.(i).(j+1) in
      let c1 = g.(i+1).(j) in
      let n1 = vnormal (b1 -^ a1) (c1 -^ a1) in
      let a2 = g.(i+1).(j+1) in
      let b2 = g.(i+1).(j) in
      let c2 = g.(i).(j+1) in
      let n2 = vnormal (b2 -^ a2) (c2 -^ a2) in
      sn.(i).(j) <- (n1, n2)
    done
  done;
  (* Now the normal vector of a vertex is the average of the normals of
   * all (up to six) adjacent triangles.
   *
   * (i+1) +-----+-----+
   *       |\n2_4|\    |
   *       | \   | \   |
   *       |  \  |  \  |
   *       |   \ |   \ |
   *       |n1_5\|n1_3\|
   *   (i) +-----+-----+
   *       |\n2_0|\n2_2|
   *       | \   | \   |
   *       |  \  |  \  |
   *       |   \ |   \ |
   *       |    \|n1_1\|
   * (i-1) +-----+-----+
   *      (j-1) (j) (j+1)
   *
   *)
  let vn = Array.make_matrix m n zero in
  for i = 0 to m - 1 do
    for j = 0 to n -1 do
      let (n2_0, f_n2_0) =
	if i>0 && j>0 then (snd sn.(i-1).(j-1), 1) else (zero, 0) in
      let (n1_1, f_n1_1) = 
	if i>0 && j<n-1 then (fst sn.(i-1).(j), 1) else (zero, 0) in
      let (n2_2, f_n2_2) =
	if i>0 && j<n-1 then (snd sn.(i-1).(j), 1) else (zero, 0) in
      let (n1_3, f_n1_3) =
	if i<m-1 && j<n-1 then (fst sn.(i).(j), 1) else (zero, 0) in
      let (n2_4, f_n2_4) =
	if i<m-1 && j>0 then (snd sn.(i).(j-1), 1) else (zero, 0) in
      let (n1_5, f_n1_5) = 
	if i<m-1 && j>0 then (fst sn.(i).(j-1), 1) else (zero, 0) in
      
      let n = n2_0 +^ n1_1 +^ n2_2 +^ n1_3 +^ n2_4 +^ n1_5 in
      let factor = f_n2_0 + f_n1_1 + f_n2_2 + f_n1_3 + f_n2_4 + f_n1_5 in
      vn.(i).(j) <- (1.0 /. float factor) *.^ n
    done
  done;
  (sn, vn)
;;


let draw_grid ~i_min ~i_max ~j_min ~j_max col pg (sn,vn) =
  GlMat.load_identity();
  GlLight.material `both (`specular col.sf_specular);
  GlLight.material `both (`shininess col.sf_shininess);
  GlDraw.color col.sf_color;
  GlDraw.polygon_mode `both `fill;
  ( match col.sf_texture with
	Some t ->
	  Gl.enable `texture_2d;
	  GlTex.env (`mode `modulate);
	  GlTex.bind_texture `texture_2d t;
      | None -> 
	  ()
  );
  GlDraw.begins `triangles;
  let (min_x, max_x, min_y, max_y) = col.sf_tex_rect in
  for i = i_min to i_max-1 do
    for j = j_min to j_max-1 do
      let (a1,na1) = (pg.(i).(j),     vn.(i).(j)) in
      let (b1,nb1) = (pg.(i).(j+1),   vn.(i).(j+1)) in
      let (c1,nc1) = (pg.(i+1).(j),   vn.(i+1).(j)) in
      let (a2,na2) = (pg.(i+1).(j+1), vn.(i+1).(j+1)) in
      let (b2,nb2) = (pg.(i+1).(j),   vn.(i+1).(j)) in
      let (c2,nc2) = (pg.(i).(j+1),   vn.(i).(j+1)) in

      let (n1,n2) = sn.(i).(j) in

      List.iter
	(fun (x,n,texc) -> 
	   GlDraw.normal3 n;
	   if col.sf_texture <> None then GlTex.coord2 texc;
	   GlDraw.vertex3 x)
	[ (a1,na1,(min_x, min_y)); 
	  (b1,nb1,(min_x, max_y)); 
	  (c1,nc1,(max_x, min_y)); 
	  (a2,na2,(max_x, max_y)); 
	  (b2,nb2,(max_x, min_y)); 
	  (c2,nc2,(min_x, max_y)) ]
    done
  done;
  GlDraw.ends();
(* -- experimental: draw the normal vectors to see whether they are right.
  GlDraw.begins `lines;
  GlDraw.normal3 (0.0, 0.0, 1.0);
  for i = 0 to m - 1 do
    for j = 0 to n - 1 do
      let x = pg.(i).(j) in
      let n = gnormal.(i).(j) in
      GlDraw.vertex3 x;
      GlDraw.vertex3 (x +@ (0.3 *@ n));
    done
  done;
  GlDraw.ends();
*)
  if col.sf_texture <> None then Gl.disable `texture_2d;
;;


class height_grid ?global_dlist (col : surface_color) p0 p1 p2 hg : auto_obj =
  (* p0, p1, p2 are three corner points of the grid (anti-clockwise):
   * 
   *   p0 +---------------------+
   *      |.....................|
   *      |.....................|
   *      |.....................|
   *   p1 +---------------------+ p2
   *
   * p1 is the logical origin of the grid, i.e.
   *  - hg.(0).(0) is the height of p1
   *  - hg.(n-1).(0) is the height of p0
   *  - hg.(0).(n-1) is the height of p2
   *
   * hg.(i).(j) > 0 means that the grid point is moved into the direction
   * of the viewer, and a negative value means that the point si moved
   * away from the viewer
   *)
  let m = Array.length hg in
  let n = Array.length hg.(0) in
  let pg = position_grid ~p:p1 ~px:p0 ~py:p2 hg in
  let (sn, vn) = normal_grid pg in
  let opm = orthoproject_tr (p0 -^ p1) (p2 -^ p1) in

  let nearest_gridpoint p =
    (* Computes an approximation which is the nearest grid point to p;
     * returns pair (i,j)
     *)
    let (qi,qj,_) = opm *::^ (p -^ p1) in
    let i = truncate(floor (qi *. float m +. 0.5)) in
    let j = truncate(floor (qj *. float n +. 0.5)) in
    (min (max i 0) (m-1), min (max j 0) (n-1))
  in
    

object(self)

  (* The visible sub grid *)
  val mutable i_min = 0
  val mutable i_max = m-1
  val mutable j_min = 0
  val mutable j_max = n-1
  val mutable dl_change = true


  method private recompute_visible_part ep =
    (* Recomputes i_min,i_max,j_min,j_max. Sets dl_change when one of the
     * new values differs more than 10 from the old value.
     * ep: eye position
     *)
    let (i0,j0) = nearest_gridpoint ep in
    (* This point is always regarded as visible. Now search in all four
     * directions...
     * TODO: Use binary search.
     * TODO: Don't look behind.
     *)
    let i_min' = ref i0 in
    while !i_min' >= 5 && vlength(pg.( !i_min'-5 ).(j0) -^ ep) < horizon do
      i_min' := !i_min' - 5
    done;
    while !i_min' >= 1 && vlength(pg.( !i_min'-1 ).(j0) -^ ep) < horizon do
      i_min' := !i_min' - 1
    done;
    let i_max' = ref i0 in
    while !i_max' < m-5 && vlength(pg.( !i_max'+5 ).(j0) -^ ep) < horizon do
      i_max' := !i_max' + 5
    done;
    while !i_max' < m-1 && vlength(pg.( !i_max'+1 ).(j0) -^ ep) < horizon do
      i_max' := !i_max' + 1
    done;
    let j_min' = ref j0 in
    while !j_min' >= 5 && vlength(pg.(i0). ( !j_min'-5 ) -^ ep) < horizon do
      j_min' := !j_min' - 5
    done;
    while !j_min' >= 1 && vlength(pg.(i0). ( !j_min'-1 ) -^ ep) < horizon do
      j_min' := !j_min' - 1
    done;
    let j_max' = ref j0 in
    while !j_max' < n-5 && vlength(pg.(i0).( !j_max'+5 ) -^ ep) < horizon do
      j_max' := !j_max' + 5
    done;
    while !j_max' < n-1 && vlength(pg.(i0).( !j_max'+1 ) -^ ep) < horizon do
      j_max' := !j_max' + 1
    done;
    let ch_i_min = abs( i_min - !i_min' ) >= 5 in
    let ch_i_max = abs( i_max - !i_max' ) >= 5 in
    let ch_j_min = abs( j_min - !j_min' ) >= 5 in
    let ch_j_max = abs( j_max - !j_max' ) >= 5 in
    if (ch_i_min || ch_i_max || ch_j_min || ch_j_max) then (
      printf "Nearest: (%d,%d)\n" i0 j0;
      printf "Update: (%d,%d,%d,%d)\n" !i_min' !i_max' !j_min' !j_max';
      flush stdout;
      i_min <- !i_min';
      i_max <- !i_max';
      j_min <- !j_min';
      j_max <- !j_max';
      dl_change <- true;
    )


  method global_dlist = global_dlist

  method point_position _ = None

  method collision t p = 
    (* Simplification: We only compare against the nearest grid point,
     * not the nearest triangle
     *)
    let (i0,j0) = nearest_gridpoint p in
    let x = pg.(i0).(j0) in       (* grid point *)
    let q = vn.(i0).(j0) in       (* normal vector of the grid point *)
    let r = p -^ x in             (* direction vector of p *)
    (* If the angle between q and s is less than 90 degrees, p is on
     * the front side (no collision), otherwise p is on the back 
     * (inner) side, and there is a collision
     *)
    let t = r *< q in
    (* When the scalar product is positive, the angle is less than 90
     * degrees.
     *)
    t < 0.0

  method update t ep = 
    dl_change <- false;
    self # recompute_visible_part ep;

  method is_visible = 
    true

  method dl_change =
    dl_change

  method draw () =
    draw_grid ~i_min ~i_max ~j_min ~j_max col pg (sn,vn)

end
;;

