OBJS = vectorops.cmo auto_obj.cmo texture.cmo height_grid.cmo weapon.cmo game.cmo
XOBJS = $(OBJS:.cmo=.cmx)
PKGS = sdl,sdl.sdlimage,lablGL

all: game

game_opt: $(XOBJS)
	ocamlfind ocamlopt -o game_opt -package "$(PKGS)" -linkpkg $(XOBJS)

game: $(OBJS)
	ocamlfind ocamlc -o game -package "$(PKGS)" -linkpkg $(OBJS)

clean:
	rm -f *.cmi *.cmo *.cmx *.o game game_opt

.SUFFIXES: .cmo .cmx .cmi .ml .mli

.mli.cmi:
	ocamlfind ocamlc -c -package "$(PKGS)" -g $<

.ml.cmo:
	ocamlfind ocamlc -c -package "$(PKGS)" -g $<

.ml.cmx:
	ocamlfind ocamlopt -c -package "$(PKGS)" $<

depend:
	ocamldep -native *.ml >depend


-include depend
