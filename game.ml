(* $Id$
 * (C) 2005 by Gerd Stolpmann. See the file LICENSE for your rights.
 *)

open Vectorops;;
open Auto_obj;;


let init_landscape() =

  let wall_col =
    { Auto_obj.surface_color with
	sf_color = (0.8, 0.8, 0.8);
	sf_specular = (1.0, 1.0, 1.0, 1.0);
	sf_shininess = 100.0;
	sf_texture = Some(Lazy.force Texture.wall_texture)
    }
  in
  let left_grid = 
    let g = Height_grid.create_grid 20 100 in
    new Height_grid.height_grid ~global_dlist: "LWall"
      wall_col 
      (-2.5, 5.0, 0.0) (-2.5, 0.0, 0.0) (-2.5, 0.0, -50.0)
      g
  in
  let right_grid = 
    let g = Height_grid.create_grid 20 100 in
    new Height_grid.height_grid ~global_dlist:"RWall"
      wall_col 
      (2.5, 5.0, -50.0) (2.5, 0.0, -50.0) (2.5, 0.0, 0.0)
      g
  in
  let floor_grid =
    let g = Array.make_matrix 2 100 (0.0) in
    new Height_grid.height_grid ~global_dlist:"Floor"
      wall_col 
      (-2.5, 0.0, 0.0) (2.5, 0.0, 0.0) (2.5, 0.0, -50.0)
      g
  in
  let ceil_grid =
    let g = Array.make_matrix 2 100 (0.0) in
    new Height_grid.height_grid ~global_dlist:"Ceiling"
      wall_col 
      (2.5, 5.0, 0.0) (-2.5, 5.0, 0.0) (-2.5, 5.0, -50.0)
      g
  in
  Auto_obj.register left_grid;
  Auto_obj.register right_grid;
  Auto_obj.register floor_grid;
  Auto_obj.register ceil_grid
;;


let desired_framerate = 20.0;;

let time_last_swap = ref 0;;

(* CHECK: Warum ist die !eye_pos mit falschem Vorzeichen? *)

let eye_pos = ref (0.0, 0.5, 0.0);;
let eye_time = ref 0;;

let eye_speed = ref (0.0, 0.0, -2.0);;    (* per second *)

let joy = lazy (Sdljoystick.open_joystick 0);;
let key = ref None ;;

let move_eye() =
  let now = Sdltimer.get_ticks() in

  let jx,jy =
    if Sdljoystick.num_joysticks() > 0 then (
      let joy = Lazy.force joy in
      Sdljoystick.update joy;
      let jx = Sdljoystick.get_axis joy 0 in
      let jy = Sdljoystick.get_axis joy 1 in
      jx,jy
    )
    else
      (0,0) in

  let jx,jy =
    match !key with
      | Some(Sdlkey.KEY_LEFT | Sdlkey.KEY_KP4) -> (-32768, 0)
      | Some(Sdlkey.KEY_RIGHT | Sdlkey.KEY_KP6) ->  (32767, 0)
      | Some(Sdlkey.KEY_UP | Sdlkey.KEY_KP8) ->     (0, 32767)
      | Some(Sdlkey.KEY_DOWN | Sdlkey.KEY_KP2) ->   (0, -32768)
      | _ -> jx,jy
  in

  let delta = now - !eye_time in
  let factor = 0.001 *. float delta in
  
  let (px, py, pz) = !eye_pos in
  let (_, _, sz) = !eye_speed in
  let sx = (float jx /. 32768.0) *. 4.0 in
  let sy = (float jy /. 32768.0) *. 4.0 in
  
  let coll p =
    let delta = 0.2 in
    List.exists 
      (fun p' -> test_collision now p') 
      [ p -^ (delta, 0.0, 0.0);
	p +^ (delta, 0.0, 0.0);
	p -^ (0.0, delta, 0.0);
	p +^ (0.0, delta, 0.0) ]
  in

  let new_eye_pos = (px +. sx *. factor,  
		     py +. sy *. factor,  
		     pz +. sz *. factor) in

  if coll new_eye_pos then (
    (* Try other types of movement *)
    let new_eye_pos1 = (px +. sx *. factor,  
			py +. sy *. factor,  
			pz) in
    if coll new_eye_pos1 then (
      let new_eye_pos2 = (px,
			  py,
			  pz +. sz *. factor) in
      if not (coll new_eye_pos2) then
	eye_pos := new_eye_pos2
    )
    else 
      eye_pos := new_eye_pos1
  )
  else
    eye_pos := new_eye_pos;

  eye_time := now
;;


let set_view w h =
  GlDraw.viewport ~x:0 ~y:0 ~w ~h;
  GlMat.mode `projection;
  GlMat.load_identity();
  GluMat.perspective ~fovy:120.0 ~aspect:(float w /. float h) ~z:(0.1, 100.0);
  GlMat.translate3 (-1.0 *.^ !eye_pos);
  (* Objekte mit z-Koordinaten von -0.1 bis -100.0 werden dargestellt *)
  GlMat.mode `modelview;
  GlClear.clear [`color; `depth];
  ()
;;


let xz_rectangle ?(show_texture = false) ~y ~xz1:(x1,z1) ~xz2:(x2,z2) () =
  GlMat.load_identity();
  if show_texture then (
    let tex = Lazy.force Texture.wall_texture in
    Gl.enable `texture_2d;
    GlTex.env (`mode `replace);
    GlTex.bind_texture `texture_2d tex;
  );
  GlDraw.begins `quads;
  GlDraw.normal3 (0.0, 1.0, 0.0);
  if show_texture then GlTex.coord2 (0.0, 0.0);
  GlDraw.vertex3 ( x1, y, z1 );
  if show_texture then GlTex.coord2 (0.0, 5.0 *. (z2-.z1));
  GlDraw.vertex3 ( x1, y, z2 );
  if show_texture then GlTex.coord2 (5.0 *. (x2-.x1), 5.0 *. (z2-.z1));
  GlDraw.vertex3 ( x2, y, z2 );
  if show_texture then GlTex.coord2 (5.0 *. (x2-.x1), 0.0);
  GlDraw.vertex3 ( x2, y, z1 );
  GlDraw.ends();
  if show_texture then
    Gl.disable `texture_2d
;;


let floor_colors col =
  GlLight.material `both (`specular (0.0, 0.0, 0.0, 1.0));
  GlLight.material `both (`shininess 1.0);
  GlDraw.color col;  (* Note: We have color material = ambient + diffuse *)
(*
  GlLight.material `both (`ambient (expand ~hom:1.0 (1.0 *@ col)));
  GlLight.material `both (`diffuse (expand ~hom:1.0 (0.5 *@ col)))
*)
;;


let redraw() =
  move_eye();

  GlMat.load_identity();
  (* GlLight.light 0 (`position (expand ~hom:1.0 !eye_pos)); *)
  GlLight.light 0 (`position (vexpand ~hom:1.0 ((0.0, 0.0, 0.0) +^ !eye_pos)));

  Auto_obj.redraw !eye_time !eye_pos;

  let pause_duration = 
    truncate (1000.0 /. desired_framerate) -  (* duration of frame *)
    (Sdltimer.get_ticks() - !time_last_swap)  (* already consumed time *)
  in
  if pause_duration > 0 then
    Sdltimer.delay pause_duration;
  
  Gl.flush();
  Sdlgl.swap_buffers();
  time_last_swap := Sdltimer.get_ticks();
;;  


let main() =
  Sdl.init [ `VIDEO; `TIMER; `JOYSTICK ];
  at_exit Sdl.quit;

  let flags = [ `OPENGL ] in

  let width = 1024 
  and height = 768 in

  let window =
    Sdlvideo.set_video_mode ~w:width ~h:height flags in

  let module SE = Sdlevent in
  SE.enable_events 
    (SE.make_mask [ SE.QUIT_EVENT;
		    SE.RESIZE_EVENT;
		    SE.EXPOSE_EVENT ;
		    SE.USER_EVENT;
		    SE.JOYBUTTONDOWN_EVENT;
                    SE.KEYDOWN_EVENT;
                    SE.KEYUP_EVENT;
		  ]);

  print_endline ("OpenGL vendor: " ^ GlMisc.get_string `vendor);
  print_endline ("OpenGL renderer: " ^ GlMisc.get_string `renderer);
  print_endline ("OpenGL version: " ^ GlMisc.get_string `version);
  print_endline ("OpenGL extensions: " ^ GlMisc.get_string `extensions);
  flush stdout;

  Gl.enable `depth_test;
  Gl.enable `lighting;
  Gl.enable `light0;
  Gl.enable `color_material;
  (* Gl.enable `fog; *)
  (* Gl.enable `cull_face; *)

  (* Enable global ambient light: *)
  GlLight.light_model (`ambient (0.1, 0.1, 0.1, 1.0));

  (* Enable local viewer: *)
  GlLight.light_model (`local_viewer true);

  GlLight.light 0 (`diffuse (8.0, 8.0, 8.0, 0.0));
  GlLight.light 0 (`specular (8.0, 8.0, 8.0, 0.0));
  GlLight.light 0 (`quadratic_attenuation 0.2);
 (* GlLight.light 0 (`spot_cutoff 70.0);  *)

  GlDraw.shade_model `smooth (* `flat *);
  (* GlDraw.cull_face `back; *)

(*
  GlLight.fog (`mode `linear);
  GlLight.fog (`start 99.0);
  GlLight.fog (`End 100.0);
  GlLight.fog (`density 0.01);
  GlLight.fog (`color (0.1, 0.1, 0.1, 1.0));
*)

  time_last_swap := Sdltimer.get_ticks();
  eye_time := Sdltimer.get_ticks();
  Sdlevent.add [SE.USER 0];

  set_view width height;
  init_landscape();

  let fire() =
    let wpos = !eye_pos +^ (0.0, -0.2, 0.0) in
    let w = new Weapon.weapon 
		~lifetime:2000 !eye_time wpos (0.0, 0.0, -5.0) in
    Auto_obj.register w
  in

  let running = ref true in
  while !running do
    SE.pump();
    let ev = SE.wait_event() in
    match ev with
	SE.QUIT ->
	  running := false
      | SE.VIDEORESIZE(w,h) ->
	  ()
      | SE.VIDEOEXPOSE ->
	  set_view width height;
	  redraw();
	  ()
      | SE.USER 0 ->
	  (* Show the next frame *)
	  set_view width height;
	  redraw();
	  Sdlevent.add [SE.USER 0];
      | SE.JOYBUTTONDOWN _ ->
           fire()
      | SE.KEYDOWN k when SE.(k.keysym = Sdlkey.KEY_SPACE) ->
           fire()
      | SE.KEYUP k when SE.(k.keysym = Sdlkey.KEY_SPACE) ->
           ()
      | SE.KEYDOWN k ->
          key := Some SE.(k.keysym)
      | SE.KEYUP _ ->
          key := None
      | _ ->
	  ()
  done
;;


Random.self_init();
main();;
