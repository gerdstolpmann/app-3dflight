(* $Id$
 * (C) 2005 by Gerd Stolpmann. See the file LICENSE for your rights.
 *)

exception Remove

type surface_color =
    { sf_color : float * float * float;
        (* ambient and diffuse color *)
      sf_specular : float * float * float * float;
      sf_shininess : float;
      sf_texture : GlTex.texture_id option;
      sf_tex_rect : float * float * float * float;
        (* min_x, max_x, min_y, max_y *)
    }

let surface_color =
  { sf_color = (1.0, 1.0, 1.0);
    sf_specular = (0.0, 0.0, 0.0, 0.0);
    sf_shininess = 1.0;
    sf_texture = None;
    sf_tex_rect = (0.0, 1.0, 0.0, 1.0);
  }

let horizon = 100.0 ;;


class type auto_obj = object
  (* An autonomous object:
   * - draws itself into the scene
   * - moves itself
   * - checks itself on collisions with other objects
   *)

  method global_dlist : string option
    (* Optionally, the auto_obj can be member of a global display list.
     * This makes only sense if the object does neither change shape
     * nor position, i.e. when it is a non-animated part of the landscape.
     * The string is the name of the global display list.
     *)

  method point_position : int -> (float * float * float) option
    (* Only actively moving objects that might collide with passive objects
     * have a point position. The argument is the time (in ms).
     *)

  method collision : int -> (float * float * float) -> bool
    (* Checks whether the object collides with the object at the passed
     * point position. The int arg is the time (in ms).
     *)

  method update : int -> (float*float*float) -> unit
    (* When the next frame must be drawn, this method is called first.
     * The int argument is the current time (in milliseconds). The
     * float triple is the position of the eye (needed for visibility
     * check).
     * The method computes the position and shape of the object, and
     * checks whether:
     * - The object still exists: If not, the method raises the exception
     *   Remove
     * - The object is currently visible: This is reflected by the value
     *   of the method is_visible.
     * - The display list must be rebuilt: This is reflected by the
     *   value of the method dl_change.
     *)

  method is_visible : bool

  method dl_change : bool

  method draw : unit -> unit
    (* Draws the object. Target is either the display list, or the
     * graphics engine.
     *)


end


let obj_registry = (Hashtbl.create 100 : 
		      (string option, auto_obj list ref) Hashtbl.t) ;;
  (* auto_obj sorted by global_dlist *)

let dl_registry = Hashtbl.create 10 ;;
  (* maps display list name to display list *)

let register obj =
  let dl_opt = obj # global_dlist in
  let obj_list = 
    try Hashtbl.find obj_registry dl_opt
    with 
	Not_found -> 
	  let l = ref [] in
	  Hashtbl.replace obj_registry dl_opt l;
	  l
  in
  obj_list := obj :: !obj_list;
;;

let redraw t ep =
  (* First update all objects. Remove objects if this is indicated *)
  Hashtbl.iter
    (fun _ l ->
       let l' =
	 List.filter
           (fun obj ->
	      try obj # update t ep; true
	      with Remove -> false
	   )
           !l in
       l := l'
    )
    obj_registry;

  (* Now redraw. For display lists, check first whether the display list
   * must be updated
   *)

  Hashtbl.iter
    (fun dl_opt l ->
       match dl_opt with
	   Some dl_name ->
	     let need_update =
	       (not (Hashtbl.mem dl_registry dl_name)) ||
	       List.exists
		 (fun obj -> obj # dl_change) 
		 !l in
	     if need_update then (
	       ( try
		   let old_dl = Hashtbl.find dl_registry dl_name in
		   GlList.delete old_dl
		 with
		     Not_found -> ()
	       );
	       let dl = GlList.create `compile_and_execute in
	       List.iter
		 (fun obj ->
		    if obj # is_visible then
		      obj # draw()
		 )
		 !l;
	       GlList.ends();
	       Hashtbl.replace dl_registry dl_name dl;
	     )
	     else
	       ( try 
		   let dl = Hashtbl.find dl_registry dl_name in
		   GlList.call dl
		 with
		     Not_found -> assert false
	       )

	 | None ->
	     (* No display list. Just draw! *)
	     List.iter
               (fun obj ->
		  if obj # is_visible then
		    obj # draw()
	       )
             !l
    )
    obj_registry;
  ()
;;


exception Collision;;

let test_collision t p =
  (* Checks whether there is an object that collides with point p
   * at time t 
   *)
  try
    Hashtbl.iter
      (fun _ l ->
	 List.iter
         (fun obj ->
	    if obj # collision t p then raise Collision
	 )
         !l
      )
      obj_registry;
    false
  with
      Collision ->
	true
;;
